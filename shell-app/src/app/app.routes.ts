import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'home',
    loadComponent: () => import('home-page/component').then(m => m.AppComponent)
  },
  {
    path: 'contact',
    loadComponent: () => import('contact-page/component').then(m => m.AppComponent)
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'home'
  }
];
